---
title: Jade
<!--subtitle: Why you'd want to hang out with me!-->
comments: false
---

Welcome to JADE's fork ! (We stand on Jade 4.5)

JADE (Java Agent DEvelopment Framework) is an open source platform for peer-to-peer agent based applications. 

JADE simplifies the implementation of multi-agent systems through a middleware that complies with the [FIPA](http://fipa.org/) specifications for interoperable intelligent multi-agent systems.

JADE provides a simple yet powerful task execution and composition model, peer-to-peer agent communication based on asynchronous message passing, a yellow pages service supporting publish subscribe discovery mechanism and many other advanced features that facilitates the development of distributed systems.

A JADE-based system can be distributed across the internet and can tranparently deploy agents on Android and J2ME-CLDC MIDP 1.0 devices. The platform configuration as well as the agent number and locations can be changed at run-time, as and when required. 
Furthermore suitable configurations can be specified to run JADE agents in networks characterized by partial connectivity including NAT and firewalls as well as intermittent coverage and IP-address changes.