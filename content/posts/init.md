
+++
title = "Jade's reloaded"
myDate = "2020-07-02"
description = "Launching Jade's second life."
+++

Hi all !

This project is a fork of the JADE Framework (4.5) motivated by the fact that the team that successfully developed it cannot continue to work on it anymore.

We can't thank them enough for the contribution they have made to the field of multi-agent systems, and we hope, by setting up this repository, that the community will be able to pursue their work on Jade and the dissemination of MAS' approaches and ideas.

 * Jade 4.5 is distributed as is and this new website currently offers the bare minimum.
 * The discord server is up and running
 * More to come

All contribution are welcome, join us [on discord](https://discord.gg/m9DgNkU) !