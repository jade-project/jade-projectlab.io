---
title: About JADE
<!--subtitle: Why you'd want to hang out with me!-->
comments: false
---

JADE (Java Agent DEvelopment Framework) is a software framework to develop agent-based applications in compliance with the FIPA specifications for interoperable intelligent multi-agent systems. The goal is to simplify the development while ensuring standard compliance through a comprehensive set of system services and agents.

JADE can be considered as an agent middleware that implements an Agent Platform and a development framework. It deals with all those aspects that are not peculiar of the agent internals and that are independent of the applications, such as message transport, encoding and parsing, or agent life-cycle.

JADE was initiated by members of the Telecom Italia Lab, the R&D branch of the Telecom Italia Group. Telecom Italia has conceived and developed JADE until JADE 4.5, and originated the Open Source Community in February 2000.

The team that successfully developed it cannot continue to work on it anymore.
We can't thank them enough for the contribution they have made to the field of multi-agent systems, and we -- with you ? -- now try to pursue their work on Jade and the dissemination of MAS' approaches and ideas.


JADE is fully implemented in Java and require jre> 1.7

JADE is released under LGPL licence.


---

## Original contributors

The Jade framework is the result of the effort of this team of people that have taken part to the JADE project during the several phases of its history:

* Fabio Bellifemine, Telecom Italia S.p.A.
<!--
Via G. Reiss Romoli, 274, 10148 – Torino
e-mail: fabioluigi.bellifemine@telecomitalia.it
-->

* Giovanni Caire, Telecom Italia S.p.A.
<!--
Via G. Reiss Romoli, 274, 10148 – Torino
<e-mail: giovanni.caire@telecomitalia.it>
-->

* Giovanni Rimassa, DII – University of Parma
<!--
Parco Area delle Scienze, 181A
e-mail: rimassa@ce.unipr.it
(Giovanni is currently with Whitestein Technologies AG Hinterbergstrasse 20 CH-6330 Cham, Switzerland)
-->

* Agostino Poggi, DII – University of Parma
<!--
Parco Area delle Scienze, 181A
<e-mail: poggi@ce.unipr.it>
-->

* Federico Bergenti, University of Parma
<!--
43124 Parma, Italy
<e-mail: federico.bergenti@unipr.it>
-->

* Tiziana Trucco, Telecom Italia S.p.A.
<!--
Via G. Reiss Romoli, 274, 10148 – Torino
<e-mail: tiziana.trucco@telecomitalia.it>
-->

* Danilo Gotta, Telecom Italia S.p.A.
<!--
Via G. Reiss Romoli, 274, 10148 – Torino
<e-mail: danilo.gotta@telecomitalia.it>
-->

* Elisabetta Cortese, Filippo Quarta, Giosuè Vitaglione, Telecom Italia S.p.A.
<!--
Via G. Reiss Romoli, 274, 10148 – Torino
<e-mail: name.surname@telecomitalia.it
(since September 2002)>
-->

## Current contributors

* [Cédric Herpson](https://twitter.com/herpsonc), Sorbonne University - CNRS, LIP6, France

* [Emmannuel Adam](http://emmanuel.adam.free.fr/site/), LAMIH - CNRS, Polytechnical University of 'Haut-De-France', France.

* You ?

## Contact

Join us on [discord !](https://discord.gg/GTwcuaGE74)
