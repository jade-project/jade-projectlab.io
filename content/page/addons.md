---
title: Add-ons
<!--subtitle: Why you'd want to hang out with me!-->
comments: false
---

## Add-ons & associated documentation

 * [Jade Web-Services Integration Gateway (WSIG) ](/docs/add-on/WSIG_Guide.pdf) - [WSIG code](/docs/add-on/wsigAddOn-4.6.zip) : The Web Service Integration Gateway add-on allows automatically exposing agent services registered with the DF as Web Services.

 * [Jade Web Services Dynamic Client (WSDC)](/docs/add-on/Jade_WSDC_Guide.pdf) - [WSDC code](/docs/add-on/wsdcAddOn-2.7.zip) : The Web Service Dynamic Client add-on allows invoking web services without any previous class generation step.

 * [Distilled StateChart Behaviour](/docs/add-on/dscAddOn.zip) : The DSC is a new JADE behaviour that allows defining agent behaviour through a particular hierarchical state machine called “Distilled StateChart (DSC)”. DSCs are obtained from the Statecharts formalism (Harel 1987) to model the agent behaviour as a set of consistent states and transitions, driven by events, to which specific actions are associated.

 * [Jade Inter-Platform Mobility Service (IPMS)](/docs/add-on/ipmsAddOn-1.5.zip) : 
 IPMS is a JADE Kernel service that allows agents to migrate between different JADE platforms. From developers point of view inter-platform mobility is identical to the ontainer-to-Container mobility natively supported by JADE. The only difference is that a PlatformID is specified as destination location instead of a ContainerID. IPMS was origially developed by the Universita Autonoma de Barcelona.


 * [Jade Gateway tutorial](/docs/add-on/JadeGateway.pdf)

 * [Jade test Suite](/docs/add-on/JADE_TestSuite.pdf)

 * [Jade OSGi ](/docs/add-on/JadeOsgi_Guide.pdf)

 * [Jade Security (Jade-S) ](/docs/add-on/JADE_Security.pdf) - [Jade-S code](docs/add-on/securityAddOn-3.10.zip) : This add-on allows deploying JADE-based system where each component (agents, containers) is owned by an authenticated user.

 * [Jade public key infrastructure (PKI) ](/docs/add-on/PKI-Guide.pdf)

 * [Jade Semantics Add-on Programmer's ](docs/add-on/SemanticsProgrammerGuide.pdf)
 

  More to come.


---
