---
title: Guides and Tutorials

comments: false
---

## Basic documentation

 * [Jade Administrator's Guide](/docs/administratorsguide.pdf) and [Jade Administration Tutorial](/page/JADEAdmin/index.html)
 * [Jade Programmer's Guide ](/docs/programmersguide.pdf) and [Jade Programming Tutorial for beginners](/docs/JADEProgramming-Tutorial-for-beginners.pdf)
 
 * [A Methodology for the Analysis and Design of Multi-Agent Systems using JADE](/docs/JADE_methodology_website_version.pdf)
 
 * [Jade languages and ontologies support](/docs/CLOntoSupport.pdf) 
 * [Jade Tutorial on the creating ontologies by means of the bean-ontology class](/docs/BeanOntologyTutorial.pdf)
 * [API](API/index.html)

## Additional Guides and Tutorials

  * [Introduction to JADE](/docs/Jade-multiagent-platform_Principles-and-main-functionalities_Herpson-2023.pdf) and [basic examples](https://startjade.gitlab.io/)
  * [Teaching resources and examples (in French)](https://emmanueladam.github.io/jade/)
  * [Jade tutorial in Portuguese](/docs/ManualJadePortuguese.pdf)

  * [FIPA and ACL standards](/docs/JADETutorial_FIPA.pdf)
  * [Running jade on Android devices](/docs/JadeAndroid-Programming-Tutorial.pdf)

